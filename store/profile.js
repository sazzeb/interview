export const state = () => ({
  profile: null,
})

export const mutations = {
  USERS_PROFILE(state, payload) {
    state.profile = payload
  },
}

export const actions = {
  async FetchUserContacts({commit}, payload ) {
    let  response
    if (payload.uid && payload.token) {
      response = await this.$axios.$post('/api/auth/password/reset/confirm/', payload)
      this.$toast.success('Successfully updated your password')
      await this.$router.push('/account/login')
    } else {
      response = await this.$axios.$post('/api/auth/password/change/', payload)
      this.$toast.success('Successfully updated your password')
    }
    if (response) {
      return response
    }
  },
  async UPDATE_USER_PROFILE({commit}, payload) {
    const response = await this.$axios.$patch('/api/', payload)
    if (response) {
      await this.$auth.fetchUser()
      return response
    }
  },
  async SEND_RESET_EMAIL({commit}, payload) {
    const response = await this.$axios.$post('/api/auth/password/reset/', payload)
    if (response) {
      return response
    }
  }
}

export const getters = {
  profile: (state) => state.profile,
}
