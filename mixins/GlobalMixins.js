import { mapGetters, mapActions } from 'vuex'
import * as rules from 'vee-validate/dist/rules'
import { extend, ValidationObserver, ValidationProvider } from 'vee-validate'

extend('email', {
  ...rules.email,

  message: (field) => {
    const rewriteWord = field.replace('_', ' ')
    return `The ${rewriteWord} entered is not a valid email address`
  },
})
extend('required', {
  ...rules.required,

  message: (field) => {
    const rewriteWord = field.replace('_', ' ')
    return `${rewriteWord} is required`
  },
})

extend('checked', {
  ...rules.required,
  message: (field) => {
    const rewriteWord = field.replace('_', ' ')
    return `${rewriteWord} must be checked before you proceed`
  },
})

extend('numeric', {
  ...rules.numeric,
  message: (field) => `The ${field} must contain only numbers`,
})

extend('min', {
  ...rules.min,
  message: (field, values) => {
    const rewriteWord = field.replace('_', ' ')
    return `The ${rewriteWord} must be greater than ${values.length} characters`
  },
})

extend('confirmed', {
  ...rules.confirmed,
  message: 'password did not match',
})

extend('max', {
  ...rules.max,
  message: (field, values) => {
    const rewriteWord = field.replace('_', ' ')
    return `The ${rewriteWord} must be less than ${values.length} characters`
  },
})
extend('password_confirmation', {
  params: ['target'],
  validate(value, { target }) {
    return value === target
  },
  message: 'Password confirmation does not match',
})
export default {
  name: 'GlobalMixins',
  components: { ValidationProvider, ValidationObserver },
  methods: {
    ...mapActions({
      SEND_RESET_EMAIL: 'profile/SEND_RESET_EMAIL',
      UPDATE_USER_PROFILE: 'profile/UPDATE_USER_PROFILE',
      FetchUserContacts: 'profile/FetchUserContacts',
    })
  },
  computed: {
    ...mapGetters({
    }),
  },
}
