module.exports = {
  purge: [
    './components/**/*.{vue,js}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      backgroundImage: {
        background: "url('~/static/img/background.svg')",
        background1: "url('~/static/img/background1.svg')",
        backgroundTopHome: "url('~/static/img/top_background.svg')"
      },
      height: {
        97: "25rem",
        100: "27rem",
        110: "30rem",
        105: "34rem",
        115: "39rem",
        120: "45rem",
        125: "60rem",
        128: "65rem",
        130: "75rem",
      },
      width: {
        100: "27rem",
        110: "30rem",
        105: "34rem",
        115: "39rem",
        120: "45rem",
        125: "60rem",
      },
      margin: {
        100: "27rem",
        110: "30rem",
        105: "34rem",
        115: "39rem",
        120: "45rem",
        125: "60rem",
        130: "75rem",
      },
      marginTop: {
        100: "27rem",
        110: "30rem",
        105: "34rem",
        115: "39rem",
        120: "45rem",
        125: "60rem",
        130: "75rem",
      },
      minWidth: {
        200: "21rem",
        12: "15rem",
        180: "40%",
      },
      maxWidth: {
        200: "84rem",
        190: "1410px",
        180: "40%",
      },
      zIndex: {
        2: 2,
        3: 4
      },
      rotate: {
        easeWard: "rotateY(-180deg)",
        westWard: "rotateY(180deg)",
      }
    },
  },
  plugins: [],
}
