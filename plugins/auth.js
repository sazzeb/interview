export default async function ({ $auth, $axios, route, store, redirect }) {
  const authStrategy = $auth.strategy.name
  if (
    authStrategy === 'facebook' ||
    authStrategy === 'linkedin' ||
    authStrategy === 'google' ||
    authStrategy === 'twitter'
  ) {
    let access, token, codes, accessToken, idToken, code
    const url = new URLSearchParams(route.hash)
    if (url) {
      access = url.get('access_token')
      if (!access) {
        access = ''
      }
      token = url.get('id_token')
      if (!token) {
        token = ''
      }
      codes = url.get('code')
      if (!codes) {
        codes = ''
      }
      accessToken = access
      idToken = token
      code = codes
      try {
        if (accessToken || idToken) {
          const url = `/api/v1/auth/${authStrategy}`
          const data = await $axios.$post(url, {
            access_token: accessToken,
            id_token: idToken,
            code,
          })
          if (data) {
            const token = {
              property: data.access_token,
              required: true,
              maxAge: 1800,
              global: true,
              type: 'Bearer',
            }
            const refreshToken = {
              property: data.refresh_token,
              data: 'refresh_token',
              maxAge: 60 * 60 * 24 * 30,
            }
            await $auth.setUserToken(token, refreshToken)
            await $auth.setStrategy('local')
            await $auth.fetchUser()
            return redirect('/account/')
          }
        }
      } catch (e) {
        return redirect('/')
      }
    }
  }
}
