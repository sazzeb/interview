import Vue from 'vue'
import GlobalMixins from './../mixins/GlobalMixins'

Vue.mixin({
  mixins: [GlobalMixins],
})
